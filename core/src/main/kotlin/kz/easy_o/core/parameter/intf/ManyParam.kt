package kz.easy_o.core.parameter.intf

interface ManyParam : Param {
    var ids: List<Long>?
}