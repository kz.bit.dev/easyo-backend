package kz.easy_o.core.parameter.session

import kz.easy_o.core.parameter.intf.Param

data class SessionSaveParam(
        var login: String?,
        var password: String?
) : Param {
    override fun toString(): String {
        return "{ login=$login, " +
                "password=$password }"
    }
}