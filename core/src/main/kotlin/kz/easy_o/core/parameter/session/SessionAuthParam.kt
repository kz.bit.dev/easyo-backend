package kz.easy_o.core.parameter.session

import kz.easy_o.core.parameter.intf.Param

data class SessionAuthParam(
        var token: String
) : Param {
    override fun toString(): String {
        return "{ token='$token' }"
    }
}