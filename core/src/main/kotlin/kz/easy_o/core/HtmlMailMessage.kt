package kz.easy_o.core

data class HtmlMailMessage(
        val subject: String,
        val htmlBody: String
) {
    override fun toString(): String {
        return "{ subject='$subject', htmlBodySize='${htmlBody.length}' }"
    }
}