package kz.easy_o.core.constant

object Constant {
    const val CONTEXT_PATH = "/api"
    const val TOKEN_SIZE = 100
    const val CODE_SIZE = 6

    object Date {
        const val JACKSON_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
        const val JACKSON_DATE_FORMAT = "yyyy-MM-dd"
        const val JACKSON_TIME_FORMAT = "HH:mm:ss"
    }
}