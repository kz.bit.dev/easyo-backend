package kz.easy_o.core.enum

enum class ResourceTypeEnum {
    IMAGE,
    VIDEO,
    FILE;
}