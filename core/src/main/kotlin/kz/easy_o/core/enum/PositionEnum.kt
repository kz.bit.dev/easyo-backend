package kz.easy_o.core.enum

enum class PositionEnum {
    TOP,
    BOTTOM,
    LEFT,
    RIGHT
}