package kz.easy_o.core.error

import kz.easy_o.core.error.intf.Error

data class FieldError(
        override val message: String,
        val field: String
) : Error