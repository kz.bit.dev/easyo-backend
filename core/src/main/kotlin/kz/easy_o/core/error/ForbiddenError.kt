package kz.easy_o.core.error

import kz.easy_o.core.enum.ErrorCodeEnum
import kz.easy_o.core.error.intf.CodeError

data class ForbiddenError(
        override val code: ErrorCodeEnum,
        override val message: String
) : CodeError