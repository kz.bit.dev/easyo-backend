package kz.easy_o.dataproviders.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EntityScan(basePackages = ["kz.easy_o.dataproviders.entity"])
@EnableJpaRepositories(basePackages = ["kz.easy_o.dataproviders.repository"])
open class DBConfig