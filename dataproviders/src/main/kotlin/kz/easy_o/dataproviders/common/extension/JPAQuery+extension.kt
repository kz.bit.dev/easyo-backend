package kz.easy_o.dataproviders.common.extension

import com.querydsl.jpa.impl.AbstractJPAQuery

fun <T, Q : AbstractJPAQuery<T, Q>, D> AbstractJPAQuery<T, Q>.fetchMap(mapper: (T) -> D): List<D> {
    return fetch().map(mapper)
}