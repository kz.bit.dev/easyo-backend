package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.SessionDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaSessionDbRepository : BaseJpaRepository<SessionDbEntity>