package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.ResourceDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository

interface JpaResourceDbRepository : BaseJpaRepository<ResourceDbEntity>