package kz.easy_o.dataproviders.repository

import kz.easy_o.dataproviders.entity.UserDbEntity
import kz.easy_o.dataproviders.repository.base.BaseJpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface JpaUserDbRepository : BaseJpaRepository<UserDbEntity> {

    @Query("SELECT u.id FROM UserDbEntity u WHERE u.phoneNumber = ?1")
    fun getIdByPhoneNumber(phoneNumber: String): Optional<Long>
}