package kz.easy_o.dataproviders.repository.base

import kz.easy_o.dataproviders.entity.base.BaseDbEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import javax.persistence.MappedSuperclass

@MappedSuperclass
interface BaseJpaRepository<T : BaseDbEntity> :
        JpaRepository<T, Long>,
        PagingAndSortingRepository<T, Long>,
        QuerydslPredicateExecutor<T>