package kz.easy_o.dataproviders.entity.converter

import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.dataproviders.entity.ResourceDbEntity
import kz.easy_o.dataproviders.entity.converter.intf.DbEntityConverter

object ResourceDbEntityConverter : DbEntityConverter<ResourceDbEntity, ResourceEntity> {
    override fun toDbEntity(entity: ResourceEntity): ResourceDbEntity {
        val resourceDbEntity = ResourceDbEntity()

        resourceDbEntity.id = entity.id
        resourceDbEntity.resourceUrl = entity.resourceUrl
        resourceDbEntity.resourceTypeEnum = entity.resourceTypeEnum

        return resourceDbEntity
    }

    override fun toEntity(dbEntity: ResourceDbEntity, isView: Boolean): ResourceEntity {
        val resourceEntity = ResourceEntity()

        resourceEntity.id = dbEntity.id
        resourceEntity.resourceUrl = dbEntity.resourceUrl
        resourceEntity.resourceTypeEnum = dbEntity.resourceTypeEnum

        return resourceEntity
    }
}