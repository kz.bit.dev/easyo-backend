package kz.easy_o.dataproviders.entity.converter.intf

import kz.easy_o.core.entity.base.BaseEntity
import kz.easy_o.dataproviders.entity.base.BaseDbEntity

interface DbEntityConverter<D : BaseDbEntity, E : BaseEntity> {
    fun toDbEntity(entity: E): D
    fun toDbEntityOptional(entity: E?): D? {
        if (entity == null) return null
        return toDbEntity(entity)
    }

    fun toEntity(dbEntity: D, isView: Boolean): E
    fun toEntityOptional(dbEntity: D?, isView: Boolean): E? {
        if (dbEntity == null) return null
        return toEntity(dbEntity, isView)
    }

    fun toEntity(dbEntity: D): E {
        return toEntity(dbEntity, false)
    }

    fun toEntityOptional(dbEntity: D?): E? {
        return toEntityOptional(dbEntity, false)
    }
}