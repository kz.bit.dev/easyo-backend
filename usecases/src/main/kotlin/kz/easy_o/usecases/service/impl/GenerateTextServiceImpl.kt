package kz.easy_o.usecases.service.impl

import kz.easy_o.usecases.service.GenerateTextService
import kotlin.random.Random

class GenerateTextServiceImpl : GenerateTextService {

    override fun getRandomToken(size: Int): String {
        return generateText(size, ('a'..'z') + ('A'..'Z') + ('0'..'9'))
    }

    override fun getRandomCode(size: Int): String {
        return generateText(size, ('0'..'9') + ('0'..'0'))
    }

    private fun generateText(size: Int, charPool: List<Char>): String {
        return (1..size)
                .map { Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("")
    }
}