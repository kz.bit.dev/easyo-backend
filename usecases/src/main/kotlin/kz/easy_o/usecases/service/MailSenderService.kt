package kz.easy_o.usecases.service

import kz.easy_o.core.HtmlMailMessage

interface MailSenderService {
    fun sendHtmlMessage(to: String,
                        mailMessage: HtmlMailMessage,
                        compilation: ((Boolean) -> (Unit))? = null)

    fun sendHtmlMessage(to: Collection<String>,
                        mailMessage: HtmlMailMessage,
                        compilation: ((Boolean) -> (Unit))? = null)
}