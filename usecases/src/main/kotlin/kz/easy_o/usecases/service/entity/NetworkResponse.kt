package kz.easy_o.usecases.service.entity

import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException

data class NetworkResponse(
        val json: String?,
        val error: HttpClientErrorException?,
        val statusCode: HttpStatus
) {
    override fun toString(): String {
        return "{ json=$json, error=$error, statusCode=$statusCode }"
    }
}