package kz.easy_o.usecases.service.impl

import kz.easy_o.core.HtmlMailMessage
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.property.AppProperty
import kz.easy_o.usecases.service.MailSenderService
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import java.util.concurrent.Executors

class MailSenderServiceImpl(
        private val javaMailSender: JavaMailSender,
        private val appProperty: AppProperty
) : MailSenderService {

    private val quickService = Executors.newScheduledThreadPool(20)
    private val logger = logger<MailSenderServiceImpl>()

    override fun sendHtmlMessage(to: String, mailMessage: HtmlMailMessage, compilation: ((Boolean) -> (Unit))?) {
        sendHtmlMessage(listOf(to), mailMessage, compilation)
    }

    override fun sendHtmlMessage(to: Collection<String>,
                                 mailMessage: HtmlMailMessage,
                                 compilation: ((Boolean) -> Unit)?) {
        logger.info("Send mail to: $to message: $mailMessage")
//        if (appProperty.profile == "local") {
//            compilation?.invoke(true)
//            return
//        }

        if (to.isEmpty()) {
            compilation?.let { it(true) }
            return
        }

        val message = javaMailSender.createMimeMessage()
        val helper = MimeMessageHelper(message, true)

        helper.setTo(to.toSet().toTypedArray())
        helper.setSubject(mailMessage.subject)
        helper.setText(mailMessage.htmlBody, true)

        quickService.submit {
            try {
                javaMailSender.send(message)
                compilation?.let { it(true) }
            } catch (ex: Exception) {
                logger.error("Exception occur while send a mail : ", ex)
                compilation?.let { it(false) }
            }
        }
        compilation?.let { it(true) }
    }
}