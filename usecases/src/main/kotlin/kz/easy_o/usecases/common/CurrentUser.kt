package kz.easy_o.usecases.common

import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.usecases.principal.UserPrincipal
import org.springframework.security.core.context.SecurityContextHolder

inline val isAuthenticated: Boolean
    get() {
        if (SecurityContextHolder.getContext().authentication == null) return false
        return SecurityContextHolder.getContext().authentication.isAuthenticated
    }

inline val authorizedUserPrincipal: UserPrincipal?
    get() {
        if (!isAuthenticated) return null
        return SecurityContextHolder.getContext().authentication.principal as UserPrincipal
    }

inline val authorizedUserEntity: UserEntity?
    get() {
        if (!isAuthenticated) return null
        return authorizedUserPrincipal?.sessionEntity?.userEntity
    }

inline val authorizedSessionEntity: SessionEntity?
    get() {
        if (!isAuthenticated) return null
        return authorizedUserPrincipal?.sessionEntity
    }