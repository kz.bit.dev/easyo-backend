package kz.easy_o.usecases.gateway

import kz.easy_o.usecases.module.session.SessionSaveUseCase
import kz.easy_o.usecases.module.user.UserByIdUseCase
import kz.easy_o.usecases.module.user.UserDeleteUseCase
import kz.easy_o.usecases.module.user.UserPageUseCase
import kz.easy_o.usecases.module.user.UserSaveUseCase

interface UserRepository :
        UserPageUseCase.UserRepository,
        SessionSaveUseCase.UserRepository,
        UserByIdUseCase.UserRepository,
        UserSaveUseCase.UserRepository,
        UserDeleteUseCase.UserRepository