package kz.easy_o.usecases.module.resource

import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class ResourceByIdUseCase(
        private val resourceRepository: ResourceRepository
) : UseCase<Long, ResourceEntity> {
    override fun execute(request: Long): ResourceEntity {
        return resourceRepository.findById(request)
                ?: throw NotFoundException("No Resource for id: $request")
    }

    interface ResourceRepository {
        fun findById(id: Long): ResourceEntity?
    }
}