package kz.easy_o.usecases.module.user

import kz.easy_o.usecases.common.UseCase

class UserDeleteUseCase(
        private val userRepository: UserRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        userRepository.deleteById(request)
    }

    interface UserRepository {
        fun deleteById(id: Long)
    }
}