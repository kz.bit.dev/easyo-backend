package kz.easy_o.usecases.module.user

import kz.easy_o.core.HtmlMailMessage
import kz.easy_o.core.entity.UserEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.common.extension.sha256
import kz.easy_o.usecases.service.MailSenderService
import kz.easy_o.usecases.validator.`object`.UserEntityValidator
import java.util.concurrent.ThreadLocalRandom
import kotlin.streams.asSequence

class UserSaveUseCase(
        private val userEntityValidator: UserEntityValidator,
        private val userRepository: UserRepository,
        private val mailSenderService: MailSenderService
) : UseCase<UserEntity, UserEntity> {
    override fun execute(request: UserEntity): UserEntity {
        userEntityValidator.checkEntity(request)

        if (request.isNewEntity()) {
            request.password = if (request.password == null) getRandomPassword() else request.password
            sendPasswordToEmail(request)
            request.password = request.password!!.sha256()
        }
        if (!request.isNewEntity() && request.password != null) {
            userRepository.updatePassword(userId = request.id!!,
                                          hashPassword = request.password!!.sha256())
            sendPasswordToEmail(request)
        }

        return userRepository.save(request)
    }

    private fun getRandomPassword(): String {
        val passwordSize = 5

        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return ThreadLocalRandom.current()
                .ints(passwordSize.toLong(), 0, charPool.size)
                .asSequence()
                .map(charPool::get)
                .joinToString("")
    }

    private fun sendPasswordToEmail(userEntity: UserEntity) {
        val subject =
                if (userEntity.isNewEntity()) "Account created"
                else "Password Changed"
        val htmlMailMessage = HtmlMailMessage(subject = subject,
                                              htmlBody = "<html>" +
                                                      "<div>" +
                                                      "login: ${userEntity.email!!}<br/>" +
                                                      "password: ${userEntity.password}" +
                                                      "</div>" +
                                                      "</html>")

        mailSenderService.sendHtmlMessage(to = userEntity.email!!,
                                          mailMessage = htmlMailMessage)
    }

    interface UserRepository {
        fun save(userEntity: UserEntity): UserEntity
        fun updatePassword(userId: Long, hashPassword: String)
    }
}