package kz.easy_o.usecases.module.session

import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.exception.NotFoundException

class SessionByIdUseCase(
        private val sessionRepository: SessionRepository
) : UseCase<Long, SessionEntity> {
    override fun execute(request: Long): SessionEntity {
        return sessionRepository.findById(request)
                ?: throw NotFoundException("No Session for id: $request")
    }

    interface SessionRepository {
        fun findById(id: Long): SessionEntity?
    }
}