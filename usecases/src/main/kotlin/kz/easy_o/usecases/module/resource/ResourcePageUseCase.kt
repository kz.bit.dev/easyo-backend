package kz.easy_o.usecases.module.resource

import kz.easy_o.core.Page
import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.core.parameter.ResourceParam
import kz.easy_o.usecases.common.UseCase

class ResourcePageUseCase(
        private val resourceRepository: ResourceRepository
) : UseCase<ResourceParam, Page<ResourceEntity>> {
    override fun execute(request: ResourceParam): Page<ResourceEntity> {
        return resourceRepository.pageByParam(request)
    }

    interface ResourceRepository {
        fun pageByParam(requestParam: ResourceParam): Page<ResourceEntity>
    }
}