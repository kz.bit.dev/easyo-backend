package kz.easy_o.usecases.module.session

import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.core.parameter.session.SessionAuthParam
import kz.easy_o.usecases.common.UseCase
import kz.easy_o.usecases.common.logger
import kz.easy_o.usecases.principal.UserPrincipal
import java.time.LocalDateTime

class SessionAuthUseCase(
        private val sessionRepository: SessionRepository
) : UseCase<SessionAuthParam, UserPrincipal?> {

    private val logger = logger<SessionAuthUseCase>()

    override fun execute(request: SessionAuthParam): UserPrincipal? {
        logger.info("Execute with request: $request")
        val sessionEntity =
                sessionRepository.findByToken(token = request.token)
                        ?: return null

        val sessionExpiredDate = sessionEntity.entryTime!!.plusDays(7)

        if (LocalDateTime.now().isAfter(sessionExpiredDate)) {
            sessionRepository.deleteById(sessionEntity.id!!)
            return null
        }

        return UserPrincipal.create(sessionEntity)
    }

    interface SessionRepository {
        fun deleteById(id: Long)
        fun findByToken(token: String): SessionEntity?
    }
}