package kz.easy_o.usecases.module.session

import kz.easy_o.usecases.common.UseCase

class SessionDeleteUseCase(
        private val sessionRepository: SessionRepository
) : UseCase<Long, Unit> {
    override fun execute(request: Long) {
        sessionRepository.deleteById(request)
    }

    interface SessionRepository {
        fun deleteById(id: Long)
    }
}