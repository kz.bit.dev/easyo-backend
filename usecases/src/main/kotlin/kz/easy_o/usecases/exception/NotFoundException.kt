package kz.easy_o.usecases.exception

class NotFoundException(message: String) : RuntimeException(message)