package kz.easy_o.usecases.property

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "sms-service")
open class ApiSmsServiceProperty {
    lateinit var login: String
    lateinit var psw: String
}