package kz.easy_o.usecases.validator.unit

import kz.easy_o.usecases.validator.unit.intf.TextValidator

object EmailAddressValidator : TextValidator {

    override fun valid(obj: String): Boolean {
        return isRegex(obj,
                       ("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})\$").toRegex())
    }
}