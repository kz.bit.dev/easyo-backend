package kz.easy_o.usecases.validator.unit.intf

interface TextValidator : UnitValidator<String> {
    fun isRegex(text: String, regex: Regex): Boolean {
        return regex.matches(text)
    }
}