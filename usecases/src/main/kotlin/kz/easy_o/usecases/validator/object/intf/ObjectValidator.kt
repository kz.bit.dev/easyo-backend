package kz.easy_o.usecases.validator.`object`.intf

import kz.easy_o.usecases.exception.FieldsException
import org.springframework.validation.DataBinder
import org.springframework.validation.Errors
import org.springframework.validation.Validator

@Suppress("UNCHECKED_CAST")
interface ObjectValidator<T> : Validator {

    val cls: Class<T>

    fun checkEntity(entity: T) {
        val dataBinder = DataBinder(entity)

        dataBinder.addValidators(this)
        dataBinder.validate()

        if (dataBinder.bindingResult.hasErrors())
            throw FieldsException.processValidationError(dataBinder.bindingResult)
    }

    override fun supports(cls: Class<*>): Boolean {
        return this.cls == cls
    }

    override fun validate(any: Any, p1: Errors) {
        validateEntity(any as T, p1)
    }

    fun validateEntity(entity: T, errors: Errors)
}