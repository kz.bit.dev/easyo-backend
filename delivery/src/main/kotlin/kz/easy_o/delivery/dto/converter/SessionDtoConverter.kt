package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.SessionEntity
import kz.easy_o.delivery.dto.SessionDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object SessionDtoConverter : DtoConverter<SessionEntity, SessionDto> {

    override fun toDto(entity: SessionEntity): SessionDto {
        val sessionDto = SessionDto()

        sessionDto.id = entity.id
        sessionDto.deleted = entity.deleted
        sessionDto.token = entity.token
        sessionDto.user = entity.userEntity?.let(UserDtoConverter::toDto)

        return sessionDto
    }

    override fun toEntity(dto: SessionDto): SessionEntity {
        val sessionEntity = SessionEntity()

        sessionEntity.id = dto.id
        sessionEntity.deleted = dto.deleted
        sessionEntity.token = dto.token
        sessionEntity.userEntity = dto.user?.let(UserDtoConverter::toEntity)

        return sessionEntity
    }
}