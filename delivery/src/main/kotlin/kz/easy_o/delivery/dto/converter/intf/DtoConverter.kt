package kz.easy_o.delivery.dto.converter.intf

import kz.easy_o.core.entity.base.BaseEntity
import kz.easy_o.delivery.dto.base.BaseDto

interface DtoConverter<E : BaseEntity, D : BaseDto> {
    fun toDto(entity: E): D
    fun toDtoOptional(entity: E?): D? {
        if (entity == null) return null
        return toDto(entity)
    }

    fun toEntity(dto: D): E
    fun toEntityOptional(dto: D?): E? {
        if (dto == null) return null
        return toEntity(dto)
    }
}