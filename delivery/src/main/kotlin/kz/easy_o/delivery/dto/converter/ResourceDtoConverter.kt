package kz.easy_o.delivery.dto.converter

import kz.easy_o.core.entity.ResourceEntity
import kz.easy_o.delivery.dto.ResourceDto
import kz.easy_o.delivery.dto.converter.intf.DtoConverter

object ResourceDtoConverter : DtoConverter<ResourceEntity, ResourceDto> {
    override fun toDto(entity: ResourceEntity): ResourceDto {
        val resourceDto = ResourceDto()

        resourceDto.id = entity.id
        resourceDto.deleted = entity.deleted
        resourceDto.resourceUrl = entity.resourceUrl
        resourceDto.resourceTypeEnum = entity.resourceTypeEnum

        return resourceDto
    }

    override fun toEntity(dto: ResourceDto): ResourceEntity {
        val resourceEntity = ResourceEntity()

        resourceEntity.id = dto.id
        resourceEntity.deleted = dto.deleted
        resourceEntity.resourceUrl = dto.resourceUrl
        resourceEntity.resourceTypeEnum = dto.resourceTypeEnum

        return resourceEntity
    }
}