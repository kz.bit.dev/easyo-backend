package kz.easy_o.delivery.dto

import kz.easy_o.delivery.dto.base.BaseDto
import java.time.LocalDateTime

data class SessionDto(
        override var id: Long? = null,
        override var deleted: Long? = null,

        var token: String? = null,
        var entryTime: LocalDateTime? = null,
        var user: UserDto? = null
) : BaseDto()