package kz.easy_o.delivery.config.module

import com.querydsl.jpa.impl.JPAQueryFactory
import kz.easy_o.dataproviders.repository.JpaResourceDbRepository
import kz.easy_o.dataproviders.repository.impl.ResourceRepositoryImpl
import kz.easy_o.delivery.resource.impl.ResourceResourceImpl
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.resource.ResourceByIdUseCase
import kz.easy_o.usecases.module.resource.ResourceDeleteUseCase
import kz.easy_o.usecases.module.resource.ResourcePageUseCase
import kz.easy_o.usecases.module.resource.ResourceSaveUseCase
import kz.easy_o.usecases.validator.`object`.ResourceEntityValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ResourceModule {

    @Bean
    fun resourceResource(useCaseExecutor: UseCaseExecutor,
                         resourcePageUseCase: ResourcePageUseCase,
                         resourceByIdUseCase: ResourceByIdUseCase,
                         resourceSaveUseCase: ResourceSaveUseCase,
                         resourceDeleteUseCase: ResourceDeleteUseCase) =
            ResourceResourceImpl(useCaseExecutor,
                                 resourcePageUseCase,
                                 resourceByIdUseCase,
                                 resourceSaveUseCase,
                                 resourceDeleteUseCase)

    @Bean
    fun resourceDeleteUseCase(resourceRepository: ResourceDeleteUseCase.ResourceRepository) =
            ResourceDeleteUseCase(resourceRepository)

    @Bean
    fun resourceByIdUseCase(resourceRepository: ResourceByIdUseCase.ResourceRepository) =
            ResourceByIdUseCase(resourceRepository)

    @Bean
    fun resourcePageUseCase(resourceRepository: ResourcePageUseCase.ResourceRepository) =
            ResourcePageUseCase(resourceRepository)

    @Bean
    fun resourceSaveUseCase(resourceEntityValidator: ResourceEntityValidator,
                            resourceRepository: ResourceSaveUseCase.ResourceRepository) =
            ResourceSaveUseCase(resourceEntityValidator,
                                resourceRepository)

    @Bean
    fun resourceEntityValidator() =
            ResourceEntityValidator()

    @Bean
    fun resourceRepository(jpaQueryFactory: JPAQueryFactory,
                           jpaResourceDbRepository: JpaResourceDbRepository) =
            ResourceRepositoryImpl(jpaQueryFactory,
                                   jpaResourceDbRepository)
}
