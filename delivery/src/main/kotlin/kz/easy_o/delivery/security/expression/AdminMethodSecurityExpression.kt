package kz.easy_o.delivery.security.expression

import org.springframework.security.access.expression.SecurityExpressionRoot
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations
import org.springframework.security.core.Authentication

class AdminMethodSecurityExpression(authentication: Authentication) : SecurityExpressionRoot(authentication),
        MethodSecurityExpressionOperations {

    private var filterObject: Any? = null
    private var returnObject: Any? = null
    private var target: Any? = null

    fun isAdmin(): Boolean {
        //        val sessionEntity = (this.principal as UserPrincipal).sessionEntity
        //        return sessionEntity.admin == true
        return true //TODO: Fix
    }

    override fun setFilterObject(filterObject: Any?) {
        this.filterObject = filterObject
    }

    override fun getFilterObject(): Any? {
        return filterObject
    }

    override fun setReturnObject(returnObject: Any?) {
        this.returnObject = returnObject
    }

    override fun getReturnObject(): Any? {
        return returnObject
    }

    override fun getThis(): Any? {
        return target
    }
}