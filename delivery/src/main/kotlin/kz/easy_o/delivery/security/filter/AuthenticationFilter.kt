package kz.easy_o.delivery.security.filter

import kz.easy_o.core.parameter.session.SessionAuthParam
import kz.easy_o.usecases.module.session.SessionAuthUseCase
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthenticationFilter(
        private val sessionAuthUseCase: SessionAuthUseCase
) : OncePerRequestFilter() {

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest,
                                  response: HttpServletResponse,
                                  filterChain: FilterChain) {
        val token = getRequestToken(request)

        if (token != null) {
            val sessionAuthParam = SessionAuthParam(token = token)

            val userPrincipal = sessionAuthUseCase.execute(sessionAuthParam)

            if (userPrincipal == null) {
                logger.warn("Not a successful login authorization: token=$token")
                filterChain.doFilter(request, response)
                return
            }

            val authentication =
                    UsernamePasswordAuthenticationToken(userPrincipal, null, userPrincipal.authorities)

            SecurityContextHolder.getContext().authentication = authentication
        }

        filterChain.doFilter(request, response)
    }

    private fun getRequestToken(request: HttpServletRequest): String? {
        val token = request.getHeader("Authorization")
        return if (StringUtils.hasText(token)) {
            return token
        }
        else null
    }
}
