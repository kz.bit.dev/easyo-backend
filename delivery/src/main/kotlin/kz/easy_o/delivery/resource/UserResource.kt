package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.UserParam
import kz.easy_o.delivery.dto.UserDto
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/users")
interface UserResource {

    @PreAuthorize("isAdmin()")
    @GetMapping
    fun page(@ModelAttribute userParam: UserParam): CompletionStage<Page<UserDto>>

    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<UserDto>

    @PostMapping
    fun save(@RequestBody userDto: UserDto): CompletionStage<UserDto>

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long,
               @RequestBody userDto: UserDto): CompletionStage<UserDto>

    @PreAuthorize("isAdmin()")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}