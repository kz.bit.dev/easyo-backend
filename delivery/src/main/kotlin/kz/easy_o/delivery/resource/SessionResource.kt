package kz.easy_o.delivery.resource

import kz.easy_o.core.Page
import kz.easy_o.core.constant.Constant
import kz.easy_o.core.parameter.SessionParam
import kz.easy_o.core.parameter.session.SessionSaveParam
import kz.easy_o.delivery.dto.SessionDto
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletionStage

@RestController
@RequestMapping(Constant.CONTEXT_PATH + "/sessions")
interface SessionResource {

    @PreAuthorize("isAdmin()")
    @GetMapping
    fun page(@ModelAttribute sessionParam: SessionParam): CompletionStage<Page<SessionDto>>

    @PreAuthorize("isAdmin()")
    @GetMapping("/{id}")
    fun byId(@PathVariable id: Long): CompletionStage<SessionDto>

    @PostMapping("/auth")
    fun create(@RequestBody sessionSaveParam: SessionSaveParam): CompletionStage<SessionDto>

    @PreAuthorize("isAdmin()")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): CompletionStage<ResponseEntity<Unit>>
}