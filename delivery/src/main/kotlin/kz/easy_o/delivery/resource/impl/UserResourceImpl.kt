package kz.easy_o.delivery.resource.impl

import kz.easy_o.core.parameter.UserParam
import kz.easy_o.delivery.common.map
import kz.easy_o.delivery.common.noContentResponse
import kz.easy_o.delivery.dto.UserDto
import kz.easy_o.delivery.dto.converter.UserDtoConverter
import kz.easy_o.delivery.resource.UserResource
import kz.easy_o.usecases.common.UseCaseExecutor
import kz.easy_o.usecases.module.user.UserByIdUseCase
import kz.easy_o.usecases.module.user.UserDeleteUseCase
import kz.easy_o.usecases.module.user.UserPageUseCase
import kz.easy_o.usecases.module.user.UserSaveUseCase

class UserResourceImpl(
        private val useCaseExecutor: UseCaseExecutor,
        private val userPageUseCase: UserPageUseCase,
        private val userByIdUseCase: UserByIdUseCase,
        private val userSaveUseCase: UserSaveUseCase,
        private val userDeleteUseCase: UserDeleteUseCase
) : UserResource {

    override fun page(userParam: UserParam) =
            useCaseExecutor(useCase = userPageUseCase,
                            requestDto = userParam,
                            requestConverter = { it },
                            responseConverter = { it.map(UserDtoConverter::toDto) })

    override fun byId(id: Long) =
            useCaseExecutor(useCase = userByIdUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { UserDtoConverter.toDto(it) })

    override fun save(userDto: UserDto) =
            useCaseExecutor(useCase = userSaveUseCase,
                            requestDto = userDto,
                            requestConverter = {
                                it.id = null
                                UserDtoConverter.toEntity(it)
                            },
                            responseConverter = { UserDtoConverter.toDto(it) })

    override fun update(id: Long, userDto: UserDto) =
            useCaseExecutor(useCase = userSaveUseCase,
                            requestDto = userDto,
                            requestConverter = {
                                it.id = id
                                UserDtoConverter.toEntity(it)
                            },
                            responseConverter = { UserDtoConverter.toDto(it) })

    override fun delete(id: Long) =
            useCaseExecutor(useCase = userDeleteUseCase,
                            requestDto = id,
                            requestConverter = { it },
                            responseConverter = { noContentResponse() })
}